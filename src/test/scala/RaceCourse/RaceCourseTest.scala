package RaceCourse

import org.scalatest.FunSuite

/**
  * Created by Nuttakron on 12/15/2016.
  */
class RaceCourseTest extends FunSuite {

  test("Create RaceCourse") {
    val totalTeam : Int = 5
    val distance : Float = 1000
    val raceCourse = new RaceCourse(totalTeam, distance)

    assert( raceCourse.getPlayer == null )
    assert( raceCourse.getFinisher.isEmpty )
  }

  test("Create RaceCourse: Play") {
    val totalTeam : Int = 5
    val distance : Float = 1000
    val raceCourse = new RaceCourse(totalTeam, distance)

    raceCourse.play()

    assert( raceCourse.getPlayer.length ==  totalTeam )
    assert( raceCourse.getFinisher.length == totalTeam )
  }

  test("Create RaceCourse: Get Result") {
    val totalTeam : Int = 1
    val distance : Float = 1
    val raceCourse = new RaceCourse(totalTeam, distance)

    raceCourse.play()

    val result = raceCourse.getResult
    val expectResult = "\uD83C\uDFC1 Result Formula F1 \uD83C\uDFC1 \nPlace: 1 is Team Number: 1 TotalDistance: 116.666664 Meter Final Speed: 62.333332 |Finish  0.0 s |\n"
    assert( result == expectResult )
  }

  /*
// This scenario shouldn't happened, because Main/ExecuteRacing should handle input
test("Create RaceCourse: Error Value") {
  val totalTeam : Int = -10
  val distance : Float = -1000
  val raceCourse = new RaceCourse(totalTeam, distance)

  raceCourse.play()

  assert( raceCourse.getPlayer == null )
  assert( raceCourse.getFinisher.isEmpty )
}
*/

}
