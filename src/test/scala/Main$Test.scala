import org.scalatest.FunSuite

/**
  * Created by Nuttakron on 12/15/2016.
  */
class Main$Test extends FunSuite {

  val defaultTotalTeam = 2
  val defaultTracks = 1000f

  def validateInput( args : Array[String], expectTotalTeam : Int, expectTracks : Float ):Unit ={
    val raceSimulator = Main.executeRacing(args)

    assert( raceSimulator.getPlayer.length == expectTotalTeam )
    assert( raceSimulator.getTracks == expectTracks )
  }

  test("Test Execute Racing with simple scenario") {
    validateInput( Array("3","3000"), 3, 3000 )
  }

  test("Test Execute Racing with both error input (Minus) ") {
    validateInput( Array("-3","-1000"), defaultTotalTeam, defaultTracks )
  }

  test("Test Execute Racing with both error input (string) ") {
    validateInput( Array("ABCDE","HELLO WORLD"), defaultTotalTeam, defaultTracks )
  }

  test("Test Execute Racing with both error input (empty) ") {
    validateInput(  Array(), defaultTotalTeam, defaultTracks )
  }

  test("Test Execute Racing with total team error") {
    validateInput( Array("-3","3000"), defaultTotalTeam, 3000 )
  }

  test("Test Execute Racing with tracks error") {
    validateInput( Array("3","-3000"), 3, defaultTracks )
  }

}
