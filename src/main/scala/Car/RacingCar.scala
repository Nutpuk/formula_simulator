package Car

/**
  * Created by Nuttakron on 12/13/2016.
  **/
class RacingCar( val teamNumber : Int  ) extends Ordered[RacingCar] {

  private var locationMeter : Float = 0.0f
  private var currentSpeed : Float = 0.0f
  private val handlingFactor : Float = 0.8f
  private val accelerationPerSecond : Float = 2f * teamNumber
  private var isUsedNitro : Boolean = false
  private var bonusSpeed : Float = 0.0f
  private var topSpeedMPS : Float = 0.0f
  private var finishTime : Float = 0.0f

  private def getStartLocation( numberFactorDistance : Float ) : Float = -200f * ( numberFactorDistance - 1 )
  private def convertKMPHToMPS( speedKMPH : Float ) : Float = speedKMPH * 1000f / 3600f

  def prepare(): RacingCar ={
    locationMeter = getStartLocation( teamNumber )
    val topSpeedKMPH  = 200f + (10f*teamNumber)
    topSpeedMPS = convertKMPHToMPS(topSpeedKMPH)

    setCurrentSpeed( topSpeedMPS )
    this
  }

  def move( ) : RacingCar = {
    locationMeter = locationMeter + ( currentSpeed + bonusSpeed )
    bonusSpeed = 0
    this
  }

  def compare(that: RacingCar) = locationMeter.compare(that.locationMeter)

  def increaseAccelerationSpeed( stepSecond : Int): RacingCar  ={
    addCurrentSpeed( increaseSpeedPerSecond(currentSpeed, stepSecond) )
    this
  }

  def useHandlingSpeed(): RacingCar ={
    setCurrentSpeed( changeSpeedToHandlingSpeed(currentSpeed) )
    println("! Team Number "+teamNumber+" is use Handling !")
    this
  }

  def useNitro(): RacingCar ={
    if( isUsedNitro == false) {
      isUsedNitro = true
      bonusSpeed =  changeSpeedToNitro(topSpeedMPS)
      println("! Team Number "+teamNumber+" is use Nitro ! Bonus Speed= "+ bonusSpeed)
    }
    this
  }

  def saveFinishTime (seconds : Float): RacingCar = {
    finishTime = seconds
    this
  }

  def getCurrentDistance: Float = locationMeter

  def getCurrentStatus: String ={
    "Team Number: "+teamNumber+
    " TotalDistance: "+locationMeter+
    " Meter Final Speed: "+currentSpeed
  }

  def getResult: String ={
    "Team Number: "+teamNumber+
      " TotalDistance: "+locationMeter+
      " Meter Final Speed: "+currentSpeed+
      " |Finish  "+ finishTime + " s |"
  }

  def startMoveFirstTime(): RacingCar ={
    move()
    this
  }

  private def addCurrentSpeed( newSpeed : Float ) : Unit = { currentSpeed += newSpeed }
  private def setCurrentSpeed( newSpeed : Float ) : Unit = { currentSpeed = newSpeed }
  private def increaseSpeedPerSecond( speed : Float, stepSecond : Int): Float = accelerationPerSecond * stepSecond
  private def changeSpeedToHandlingSpeed(speed : Float):  Float = speed * handlingFactor
  private def changeSpeedToNitro( speed : Float): Float =  speed * 2

}
