import RaceCourse.RaceCourse

import scala.util.Try

/**
  * Created by Nuttakron on 12/14/2016.
  */

object Main {

  def executeRacing( args:  Array[String] ): RaceCourse = {
    println(" !! Welcome to Racing Simulator !!")

    var totalTeam : Int = 2
    var goalDistanceMeter : Float = 1000f
    if( Try{ args(0).toInt }.isSuccess && args(0).toInt > 0 ) totalTeam = args(0).toInt
    else println("Warning ! Your input team is error, use default total team = 2")
    if( Try{ args(1).toInt }.isSuccess && args(1).toInt > 0  ) goalDistanceMeter = args(1).toInt
    else println("Warning ! Your input distance is error, use default total distance = 1000")


    val raceSimulator = new RaceCourse(totalTeam, goalDistanceMeter)
    raceSimulator.play()
    println( raceSimulator.getResult )

    raceSimulator
  }

  def main( args:  Array[String]): Unit ={
    executeRacing(args)
  }


}