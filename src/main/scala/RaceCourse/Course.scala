package RaceCourse

import Car.RacingCar

import scala.collection.mutable.ListBuffer

/**
  * Created by Nuttakron on 12/13/2016.
  */
class Course( listRacingCar : Array[RacingCar], goalDistanceMeter : Float ) {

  private def onSetup( listCar : Array[RacingCar]) : Array[RacingCar] = listCar.map(  _.prepare() )

  private def adjustAccelerationSpeed( listCar : Array[RacingCar], stepTimeSecond : Int): Array[RacingCar] = listCar.map(  _.increaseAccelerationSpeed(stepTimeSecond) )

  private def sortOrderRacingCar( listCar : Array[RacingCar]) : Array[RacingCar] ={
    val listSortCar = listCar.clone()
    scala.util.Sorting.quickSort(listSortCar)
    val listSortCarDesc = listSortCar.reverse
    listSortCarDesc
  }

  private def firstPlaceCheckDistance( listCar : Array[RacingCar] ): Array[RacingCar] = {

    if( listCar.length > 1) {
      val firstPlace = listCar(0)
      val secondPlace = listCar(1)
      val isSafeDistance = (firstPlace.getCurrentDistance - secondPlace.getCurrentDistance) >= 10f
      if (isSafeDistance) {
        firstPlace.useHandlingSpeed()
      }
    }

    listCar
  }

  private def lastPlaceCheckDistance( listCar : Array[RacingCar] ): Array[RacingCar] = {
    listCar( listCar.length-1 ).useNitro()
    listCar
  }

  private def moveCar (listCar: Array[RacingCar]) : Array[RacingCar] = listCar.map( _.move() )

  private def addFinisher( listFinisher : ListBuffer[RacingCar], listParticipate : Array[RacingCar], goalDistanceMeter : Float, finishTime : Float ) : ListBuffer[RacingCar] = {

    for( car <- listParticipate ){
      if(car.getCurrentDistance >= goalDistanceMeter ) {
        listFinisher += car.saveFinishTime(finishTime)
      }
    }

    listFinisher
  }

  private def removeFinisher( listParticipate : Array[RacingCar], goalDistanceMeter : Float ) : Array[RacingCar] = {

    val listRemain = listParticipate.filter( _.getCurrentDistance < goalDistanceMeter )

    listRemain
  }

  private def startMoving( listParticipate: Array[RacingCar]): Array[RacingCar] = listParticipate.map( _.startMoveFirstTime() )

  private def startRacing( listCar : Array[RacingCar], goalDistance : Float, stepTimeSecond: Int ) : ListBuffer[RacingCar] = {

    var listFinisher = new ListBuffer[RacingCar]
    var listParticipate = listCar.clone()
    var times : Float = 0.0f

    listParticipate = startMoving(listParticipate)
    var listParticipateSort = sortOrderRacingCar(listParticipate)

    //Simulator loop assume check every 2 sec
    while( listFinisher.length < listCar.length ) {

      println("======== Running "+ times +" s ==============")
      println(">>>> Prepare Move")
      for( car <- listParticipateSort ) println(car.getCurrentStatus)
      listParticipateSort = moveCar(listParticipateSort)

      println(">>>> Event")
      listParticipateSort = adjustAccelerationSpeed(listParticipateSort, stepTimeSecond)
      listParticipateSort = firstPlaceCheckDistance(listParticipateSort)
      listParticipateSort = lastPlaceCheckDistance(listParticipateSort)

      listParticipateSort = sortOrderRacingCar(listParticipateSort)
      listFinisher = addFinisher(listFinisher, listParticipateSort, goalDistance, times )
      listParticipateSort = removeFinisher(listParticipateSort, goalDistance)

      println(">>>> Moved")
      for( car <- listParticipateSort ) println(car.getCurrentStatus)
      times+= stepTimeSecond
    }

    listFinisher
  }

  def Start() : ListBuffer[RacingCar] ={
    val stepTimeSecond = 2
    val listParticipate = onSetup( listRacingCar )
    val listFinisher = startRacing( listParticipate, goalDistanceMeter, stepTimeSecond)
    listFinisher
  }

}
