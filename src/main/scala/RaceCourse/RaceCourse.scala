package RaceCourse

import Car.RacingCar

import scala.collection.mutable.ListBuffer

/**
  * Created by Nuttakron on 12/13/2016.
  */
class RaceCourse(temNumber : Int, tracks : Float){

  private var listTeam : Array[RacingCar] = _
  private var listFinisher = new ListBuffer[RacingCar]

  def getPlayer: Array[RacingCar] = listTeam
  def getFinisher: ListBuffer[RacingCar] = listFinisher
  def getTracks : Float = tracks

  private def setup( totalTeam : Int ): Array[RacingCar] ={

    var teams = new Array[RacingCar](totalTeam)
    var teamIdx = 0
    teams = teams.map( x => {
      teamIdx += 1
      new RacingCar(teamIdx)
    } )

    teams
  }

  private def startCourse( listTeamParticipate : Array[RacingCar]) : ListBuffer[RacingCar] ={
    val course = new Course(listTeamParticipate, getTracks)
    val listPlayerFinish = course.Start()
    listPlayerFinish
  }

  def getResult: String ={
    var result = ""
    val resultTitle = "\uD83C\uDFC1 Result Formula F1 \uD83C\uDFC1 \n"
    var resultRacing = ""
    //println(resultTitle)
    var placeNum = 1
    for( car <- listFinisher ) {
      val resultPlace = "Place: " + placeNum + " is " + car.getResult + "\n"
      resultRacing +=  resultPlace
      //println(resultPlace)
      placeNum+=1
    }

    result += ( resultTitle + resultRacing )
    result
  }

  def play(): Unit ={
    listTeam = setup( temNumber )
    listFinisher = startCourse(listTeam)
  }

}
